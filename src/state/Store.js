import {observable, computed} from 'mobx';

class Store{
  constructor() {
    this.pokemons = null;
    this.pokemonIndividuals = {};

    this.pokemonAllComments = {};
    this.userAndComm = {};

    this.newComment = '';

    this.pokemonLikes = {};
  }
}

export default observable.object(new Store());
