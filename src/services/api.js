import {save, read, remove} from './storage';
import store from '../state/Store';
import {runInAction} from 'mobx';

export function getPokemons() {
  if(store.pokemons !== null) return new Promise((resolve) =>resolve()).then(() => store.pokemons);
  return fetch('https://pokedex.byinfinum.co/api/v1/pokemons', {
    method: 'GET',
    headers: {
      'Authorization': `Token token=${read('auth-token')}, email=${read('email')}`
    }
  }).then((res) => res.json())
    .then((response) => {
      runInAction(() => {
        store.pokemons = response.data;
        store.pokemons = JSON.parse(JSON.stringify(store.pokemons));
      });
      return response.data;
    });
}

export function getPokemon(id) {
  if(store.pokemonIndividuals[id]) return new Promise((resolve) =>resolve()).then(() => store.pokemonIndividuals[id]);
  return fetch(`https://pokedex.byinfinum.co/api/v1/pokemons/${id}`, {
    method: 'GET',
    headers: {
      'Authorization': `Token token=${read('auth-token')}, email=${read('email')}`
    }
  }).then((res) => res.json())
    .then((response) => {
      runInAction(() => {
        store.pokemonIndividuals[id] = response.data;
        store.pokemonIndividuals = JSON.parse(JSON.stringify(store.pokemonIndividuals));
      });
      return response.data;
    });
}

export function login(email, password){
  const bodyData = {
    'data': {
      'type': 'session',
      'attributes': {
        'email': email,
        'password': password
      }
    }
  };
  return fetch('https://pokedex.byinfinum.co/api/v1/users/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(bodyData)
  }).then((res) => res.json())
    .then((resp) => {
      if(!resp.errors){
        save("auth-token", resp.data.attributes["auth-token"]);
        save("username", resp.data.attributes["username"]);
        save("email", resp.data.attributes["email"]);
      }
      return resp;
    });
}

export function logout(){
  return fetch('https://pokedex.byinfinum.co/api/v1/users/logout', {
    method: 'DELETE',
    headers: {
        'Authorization': `Token token=${read('auth-token')}, email=${read('email')}`
    },
  })
  .then(() => {
    remove("auth-token");
    remove("username");
    remove("email");
  });
}

export function register(username, email, password, passwordconf){
  const bodyData = {
    'data': {
        'type': 'users',
        'attributes': {
          'username': username,
          'email': email,
          'password': password,
          'password_confirmation': passwordconf
        }
    }
  }

  return fetch('https://pokedex.byinfinum.co/api/v1/users', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(bodyData)
  }).then((res) => res.json());
}

export function createComment(comment, id){
  const bodyData = {
    "data": {
      "attributes": {
        "content": comment
      }
    }
  }

  return fetch(`https://pokedex.byinfinum.co/api/v1/pokemons/${id}/comments`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Token token=${read('auth-token')}, email=${read('email')}`
    },
    body: JSON.stringify(bodyData)
  }).then((res) => res.json())
    .then((response) => {
      runInAction(() => {
        store.pokemonAllComments[id] = null;
        store.userAndComm[id] = [];
      })
    })
    .then(() => getComments(id));
}

export function getUser(id){
  return fetch(`https://pokedex.byinfinum.co/api/v1/users/${id}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Token token=${read('auth-token')}, email=${read('email')}`
    }
  }).then((res) => res.json());
}

export function getComments(id){
  if(store.pokemonAllComments[id]) return new Promise((resolve) =>resolve()).then(() => store.pokemonAllComments[id]);
  return fetch(`https://pokedex.byinfinum.co/api/v1/pokemons/${id}/comments`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Token token=${read('auth-token')}, email=${read('email')}`
    }
  }).then((res) => res.json())
    .then((response) => {
      runInAction(() => {
        store.pokemonAllComments[id] = response;
        store.pokemonAllComments = JSON.parse(JSON.stringify(store.pokemonAllComments));
      });
      return response;
    });
}
export function upVote(id){
  return fetch(`https://pokedex.byinfinum.co/api/v1/pokemons/${id}/upvote`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Token token=${read('auth-token')}, email=${read('email')}`
    }
  }).then((res) => res.json())
    .then((response) => {
      runInAction(() => {
        if(store.pokemons)
          store.pokemons.filter((pokemon) => pokemon.id === id)[0].attributes['voted-on'] = 1;
        store.pokemonLikes[id] = 1;
        store.pokemonLikes = JSON.parse(JSON.stringify(store.pokemonLikes));
        store.pokemons = JSON.parse(JSON.stringify(store.pokemons));
      });
      return response;
    });
}

export function downVote(id){
  return fetch(`https://pokedex.byinfinum.co/api/v1/pokemons/${id}/downvote`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Token token=${read('auth-token')}, email=${read('email')}`
    }
  }).then((res) => res.json())
    .then((response) => {
      runInAction(() => {
        if(store.pokemons)
          store.pokemons.filter((pokemon) => pokemon.id === id)[0].attributes['voted-on'] = -1;
        store.pokemonLikes[id] = -1;
        store.pokemonLikes = JSON.parse(JSON.stringify(store.pokemonLikes));
        store.pokemons = JSON.parse(JSON.stringify(store.pokemons));
      });
      return response;
    });
}
