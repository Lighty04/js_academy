import React, {Component} from 'react';
import Like from '../../assets/thumb_up.png';
import Dislike from '../../assets/thumb_down.png';
import LikeSelected from '../../assets/thumb_up_selected.png';
import DislikeSelected from '../../assets/thumb_down_selected.png';
import {upVote, downVote} from '../../services/api';
import './PokemonItem.css';

const PokemonItem = ({pokemon: {attributes}, pokemonId, pokemonLikes }) => (
  <div className="pokemon-item__container">
    <h3 className="pokemon-item__name" >{attributes.name.charAt(0).toUpperCase() + attributes.name.slice(1)}</h3>

    <div className="pokemon-item__avatar-and-about">
      <div>
        <img className="pokemon-item__avatar" src={`https://pokedex.byinfinum.co/${attributes['image-url']}`} />
      </div>
      <div>
        <p className="pokemon-item__about-and-info-text">ABOUT</p>
        <div>
          <p className="pokemon-item__description">{attributes["description"]} Here is some text about nothing so it looks little better until description updates.</p>
        </div>
        <p className="pokemon-item__about-and-info-text">INFO</p>
        <div className="pokemon-item__pokemon-size">
          <div><p className="pokemon-item__info-text">HEIGHT</p><p>{attributes['height']}</p></div>
          <div><p className="pokemon-item__info-text">WEIGHT</p><p>{attributes['weight']}</p></div>
        </div>
        <div className="pokemon-item__likes">
          <img className="pokemon-item__image-likes" src={pokemonLikes[pokemonId] === 1 ? LikeSelected : Like} onClick={() => upVote(pokemonId)}/>
          <img className="pokemon-item__image-likes" src={pokemonLikes[pokemonId] === -1 ? DislikeSelected : Dislike} onClick={() => downVote(pokemonId)}/>
        </div>
        <button className="pokemon-item__choose-button" >CHOOSE POKEMON</button>
      </div>
    </div>

  </div>
);

export default PokemonItem;
