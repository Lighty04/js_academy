import React from 'react';
import PokemonLogoURL from '../../assets/logo.png';
import PokeballURL from '../../assets/pokeball_ball.png';
import './Register.css';

const Register = ({emailChange, usernameChange, passwordChange, passwordConfirmationChange, sendRegistrationData, errors}) => (
  <div className="register__container">
    <img className="register_logo" src={PokemonLogoURL} />
    <img src={PokeballURL} />
    <form className="register__register-form" action="" onSubmit={sendRegistrationData}>
      <input className="register__inputs" type="text" placeholder="E-mail" onChange={emailChange} />
      <input className="register__inputs" type="text" placeholder="Username" onChange={usernameChange} />
      <input className="register__inputs" type="password" placeholder="Password" onChange={passwordChange} />
      <input className="register__inputs" type="password" placeholder="Password confirmation" onChange={passwordConfirmationChange} />
      <button className="register__register" type="submit">REGISTER</button>
    </form>
    <p>{errors.map((el) => <div>{el}</div>)}</p>
  </div>
)

export default Register;
