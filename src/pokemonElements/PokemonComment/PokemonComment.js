import React from 'react';
import store from '../../state/Store';
import {observer} from 'mobx-react';
import './PokemonComment.css';

const PokemonComment = ({pokemonId, onCommentSubmit, newCommentChange}) => (
      <div className="pokemon-comment__container">
        <h1>COMMENTS</h1>
        <div className="pokemon-comment__comments">
        {
          store.userAndComm[pokemonId] && store.userAndComm[pokemonId].length !== 0 ?
          (store.userAndComm[pokemonId].map((elem) =>
            <div className="pokemon-comment__name-content-date">
              <div>
                <p>{`${elem.name}`}</p>
                <p>{`${elem.comment.content}`}</p>
              </div>
              <div>
                <p>{`${elem.comment['created-at'].split("T")[0]}`}</p>
              </div>
            </div>)) :
            null
        }
        </div>
        <div>
          <form className="pokemon-comment__comment-form" onSubmit={onCommentSubmit}>
            <input className="pokemon-comment__comment-input" onChange={newCommentChange} type="text" placeholder="Insert comment"/>
            <button className="pokemon-comment__comment-post">POST</button>
          </form>
        </div>
      </div>
)

export default observer(PokemonComment);
