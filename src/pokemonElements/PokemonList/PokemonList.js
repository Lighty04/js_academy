import React from 'react';
import {Link} from 'react-router';
import {upVote, downVote} from '../../services/api';
import Like from '../../assets/thumb_up.png';
import Dislike from '../../assets/thumb_down.png';
import LikeSelected from '../../assets/thumb_up_selected.png';
import DislikeSelected from '../../assets/thumb_down_selected.png';
import './PokemonList.css';

const PokemonList = ({pokemons}) => (
  <div className="pokemon-list__title-container">
    <p className="pokemon-list__title">Pokedex</p>
    <div className="pokemon-list__container">
      {
          pokemons.map((pokemon) => (
          <div className="pokemon-list__pokeitem">
            <Link to={`/pokemon/${pokemon['id']}`}>
              <img className='pokemon-list__pokeavatar' src={`https://pokedex.byinfinum.co/${pokemon.attributes['image-url']}`} />
            </Link>
            <div className="pokemon-list__likes">
              <p>{pokemon.attributes['name'].charAt(0).toUpperCase() + pokemon.attributes['name'].slice(1)}</p>
              <div>
                <img className="pokemon-list__image-likes" src={pokemon.attributes['voted-on'] === 1 ? LikeSelected : Like} onClick={() => upVote(pokemon.id)}/>
                <img className="pokemon-list__image-likes" src={pokemon.attributes['voted-on'] === -1 ? DislikeSelected : Dislike} onClick={() => downVote(pokemon.id)}/>
              </div>
            </div>
            <rev>{pokemon.attributes['description']}</rev>
          </div>
        )).slice(0, -2).reverse()
      }
    </div>
  </div>
)

export default PokemonList;
