import React from 'react';
import PokemonLogoURL from '../../assets/logo.png';
import PokeballURL from '../../assets/pokeball_ball.png';
import {Link} from 'react-router';

import './Login.css';

const Login = ({emailChange, passwordChange, loginChange, loginMessage}) => (
  <div className="login__container">
      <img className="login__logo" src={PokemonLogoURL} />
      <img src={PokeballURL} />
      <form className="login__login-form" action="" onSubmit={loginChange}>
        <input className="login__inputs" onChange={emailChange} placeholder="E-mail" />
        <input className="login__inputs" onChange={passwordChange} type="password" placeholder="Password" />
        <button className="login__login" type="submit">LOGIN</button>
      </form>
      <Link className="login__register" to="/register">Register</Link>
      <p>{loginMessage}</p>
  </div>
);

export default Login;
