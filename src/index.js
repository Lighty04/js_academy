import React from 'react';
import {render} from 'react-dom';
import {Router, Route, browserHistory, IndexRoute} from 'react-router';

import PokemonDetailsPage from './pages/PokemonDetailsPage/PokemonDetailsPage';
import RegisterPage from './pages/RegisterPage/RegisterPage';
import LoginPage from './pages/LoginPage/LoginPage';
import PokemonListPage from './pages/PokemonListPage/PokemonListPage';
import HeaderContainer from './pages/HeaderContainer/HeaderContainer';

import './index.css';

import {useStrict} from 'mobx';

useStrict(true);
render(
  <Router history={browserHistory}>
     <Route path="/" component={LoginPage} />
     <Route path="/pokemon" component={HeaderContainer}>
       <IndexRoute component={PokemonListPage} />
       <Route path=":id" component={PokemonDetailsPage} />
     </Route>
     <Route path="/register" component={RegisterPage} />
   </Router>
   , document.querySelector('.js-app')
);
