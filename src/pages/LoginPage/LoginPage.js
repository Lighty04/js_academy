import React, {Component} from 'react';
import {browserHistory} from 'react-router';
import {read} from '../../services/storage';
import {login} from '../../services/api';
import Login from '../../pokemonElements/Login/Login';
import './LoginPage.css';

class LoginPage extends Component{

  componentWillMount(){
    if(read("auth-token")) {
      browserHistory.push('/pokemon');
    }

    this.state = {
      email: '',
      password: '',
      loginMessage: ''
    };

    this.loginChange = this.loginChange.bind(this);
    this.emailChange = this.emailChange.bind(this);
    this.passwordChange = this.passwordChange.bind(this);
  }

  emailChange(event){
    this.setState({email: event.target.value})
  }

  passwordChange(event){
    this.setState({password: event.target.value})
  }

  loginChange(event){
    event.preventDefault();
    login(this.state.email, this.state.password)
      .then((resp) => {
          resp.errors ?
          this.setState({loginMessage: resp.errors[0].detail.charAt(0).toUpperCase() + resp.errors[0].detail.slice(1)}) :
          browserHistory.push('/pokemon');
      });
  };

  render(){
    return (
      <Login
        emailChange={this.emailChange}
        passwordChange={this.passwordChange}
        loginChange={this.loginChange}
        loginMessage={this.state.loginMessage}
      />
    );
  }

}
export default LoginPage;
