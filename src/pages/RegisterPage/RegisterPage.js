import React, {Component} from 'react';
import {Link, browserHistory} from 'react-router';
import {read} from '../../services/storage';
import {register} from '../../services/api';
import PokemonLogoURL from '../../assets/logo.png';
import PokeballURL from '../../assets/pokeball_ball.png';
import Register from '../../pokemonElements/Register/Register';
import './RegisterPage.css';

class RegisterPage extends Component {

  componentWillMount(){
    if(read("auth-token")) {
      browserHistory.push('/pokemon');
    }

    this.state = {
      errors: [],
      username: "",
      email: "",
      pass: "",
      passConf: ""
    };

    this.emailChange = this.emailChange.bind(this);
    this.usernameChange = this.usernameChange.bind(this);
    this.passwordChange = this.passwordChange.bind(this);
    this.passwordConfirmationChange = this.passwordConfirmationChange.bind(this);
    this.sendRegistrationData = this.sendRegistrationData.bind(this);
  }

  emailChange(event){
    this.setState({email: event.target.value})
  }

  usernameChange(event){
    this.setState({username: event.target.value})
  }

  passwordChange(event){
    this.setState({pass: event.target.value})
  }

  passwordConfirmationChange(event){
    this.setState({passConf: event.target.value})
  }

  sendRegistrationData(event){
    event.preventDefault();
    const {username, email, pass, passConf} = this.state;
    register(username, email, pass, passConf)
    .then((resp) => {
      if(resp.errors){
        this.setState({
          errors: resp.errors.map((err) => `Field ${err.source.pointer.split('/').pop()}: ${err.detail}`)
        });
      } else {
        this.setState({
          errors: ["Registration request is sent."]
        });
        setTimeout(() => browserHistory.push('/'), 1000);
      }
    });
  };

  render() {
    return (
      <Register
        emailChange={this.emailChange}
        usernameChange={this.usernameChange}
        passwordChange={this.passwordChange}
        passwordConfirmationChange={this.passwordConfirmationChange}
        sendRegistrationData={this.sendRegistrationData}
        errors={this.state.errors}
      />
    );
  };

}

export default RegisterPage;
