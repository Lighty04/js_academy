import React, {Component} from 'react';
import {Link} from 'react-router';
import {browserHistory} from 'react-router';

import {read, remove} from '../../services/storage';
import {logout} from '../../services/api';

import './HeaderContainer.css';

import PokemonLogoURL from '../../assets/logo.png'

class HeaderContainer extends Component {

  componentWillMount(){
    if(!read("auth-token")) {
      browserHistory.push('/');
    }

    this.logoutChange = this.logoutChange.bind(this);
  }

  logoutChange(){
    logout().then(() => {
      browserHistory.push('/');
    });
  }

  render() {
    return(
      <div>
        <div className="header-container__header">
          <Link to="/"><img className="" src={PokemonLogoURL} /></Link>
          <div>
            <button className="header-container__logout" onClick={() => browserHistory.push('/pokemon')}>Pokedex</button>
            <button className="header-container__logout" onClick={() => this.logoutChange()}>Logout</button>
          </div>
        </div>
        <div>
          {this.props.children}
        </div>
      </div>);
  };

}

export default HeaderContainer;
