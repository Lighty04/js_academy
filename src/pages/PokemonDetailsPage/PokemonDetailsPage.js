import React, {Component} from 'react';

import {getPokemon, getComments, createComment} from '../../services/api';
import {read} from '../../services/storage';

import PokemonItem from '../../pokemonElements/PokemonItem/PokemonItem';
import PokemonComment from '../../pokemonElements/PokemonComment/PokemonComment';

import {observable, runInAction} from 'mobx';
import {observer} from 'mobx-react';
import store from '../../state/Store';

import './PokemonDetailsPage.css';

@observer
class PokemonDetailsPage extends Component {

  componentWillMount() {
    this.pokemonId = this.props.params.id;

    getPokemon(this.pokemonId)
    .then(() => getComments(this.pokemonId))
    .then(() => {
      runInAction(() => {
        if(!store.pokemonLikes[this.pokemonId])
          store.pokemonLikes[this.pokemonId] = store.pokemonIndividuals[this.pokemonId].attributes['voted-on'];
        store.newComment = '';
        this.connectUsersAndComments();
      });
    })

    this.onCommentSubmit = this.onCommentSubmit.bind(this);
  }

  connectUsersAndComments(){
    store.userAndComm[this.pokemonId] = [];
    store.pokemonAllComments[this.pokemonId].data.forEach((comment) => {
      store.pokemonAllComments[this.pokemonId].included.forEach((author) => {
        if(comment.relationships.author.data.id === author.id){
          store.userAndComm[this.pokemonId].push({
            name: author.attributes.username,
            comment: comment.attributes
          });
        }
      })
    })

    store.pokemonAllComments = JSON.parse(JSON.stringify(store.pokemonAllComments));
    store.userAndComm = JSON.parse(JSON.stringify(store.userAndComm));
  };

  onCommentSubmit(event){
    event.preventDefault();
    createComment(store.newComment, this.pokemonId)
    .then(() => runInAction(() => this.connectUsersAndComments()))
    .then(() => document.querySelector('.pokemon-comment__comment-input').value = '');
  };

  newCommentChange(event){
    runInAction(() => store.newComment = event.target.value);
  }

  render(){
    return(
      store.pokemonIndividuals[this.pokemonId] && store.pokemonAllComments[this.pokemonId] ?
      (<div className="pokemon-details__container">
        <PokemonItem
          pokemon={store.pokemonIndividuals[this.pokemonId]}
          pokemonId={this.pokemonId}
          pokemonLikes={store.pokemonLikes}
        />
        <PokemonComment
          comments={store.pokemonAllComments[this.pokemonId]}
          pokemonId={this.pokemonId}
          onCommentSubmit={this.onCommentSubmit}
          newCommentChange={this.newCommentChange}
        />
      </div>) :
      null
    );
  }

}

export default PokemonDetailsPage;
