import React, {Component} from 'react';
import {getPokemons} from '../../services/api';
import PokemonList from '../../pokemonElements/PokemonList/PokemonList';
import {read} from '../../services/storage';
import {browserHistory} from 'react-router';

import store from '../../state/Store';
import {observable} from 'mobx';
import {observer} from 'mobx-react';

@observer
class PokemonListPage extends Component {

  componentWillMount() {
    getPokemons();
  }

  render() {
    return (
      !store.pokemons ?
      null :
      (<div>
        <PokemonList pokemons={store.pokemons}/>
      </div>)
    );
  }

}
export default PokemonListPage;
